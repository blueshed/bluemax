""" These tasks are used in development """
import os
from shutil import rmtree
from invoke import task


@task
def bump_version(ctx, part, confirm=False):
    """ move on the next version """
    if confirm:
        ctx.run("bumpversion {part}".format(part=part))
    else:
        ctx.run(
            "bumpversion --dry-run --allow-dirty --verbose {part}".format(part=part)
        )
        print('Add "--confirm" to actually perform the bump version.')


@task
def clean(_):
    """ tidy up """
    if os.path.isdir("build"):
        rmtree("build")
    if os.path.isdir("dist"):
        rmtree("dist")


@task(pre=[clean])
def release(ctx, version="patch"):
    """ release to pypi """
    ctx.run(f"inv bump-version --confirm {version}")
    ctx.run("pip install -e .[dev]")
    ctx.run("python setup.py sdist bdist_wheel")
    ctx.run("twine upload dist/*")


@task
def docs(ctx):
    """ build docs """
    ctx.run("cd docs && make html")


@task
def lint(ctx):
    """ lint the project package """
    ctx.run("flake8 bluemax")
    ctx.run("pylint bluemax")


@task(pre=[lint])
def test(ctx):
    """ run test with coverage """
    ctx.run("pytest --cov=bluemax tests")


@task
def report(ctx):
    """ display our coverage report """
    ctx.run("coverage report -m")
