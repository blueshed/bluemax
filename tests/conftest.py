# pylint: disable=W0621
"""
    set up for our tests
"""
import os
from urllib.parse import urlencode
import pytest
import redis
from tornado import httpclient
from tornado.web import RequestHandler
from bluemax.web.server import make_app
from bluemax.web.authentication.profile_handler import ProfileHandler
from bluemax.sa.db_session import session_scope
from bluemax import settings


class RootHandler(RequestHandler):  # pylint: disable=W0223
    """ Simple handler """

    def get(self):
        """ salute """
        self.write("hello")


@pytest.fixture
def app():
    settings.SETTINGS.clear()
    settings.SETTINGS.update(
        {
            "procedures": "tests.foo.procedures",
            "tornado": {
                "auth_handler": "tests.foo.urls:SampleAuth",
                "manager": "bluemax:ContextRpc",
                "static_dir": None,
            },
            "redis_url": None,
        }
    )
    result = make_app()
    result.add_handlers(r".*", [(r"/", RootHandler)])
    result.add_handlers(r".*", [(r"/profile", ProfileHandler)])
    return result


@pytest.fixture
async def auth_cookies(http_server_client):
    data = urlencode({"username": "foo", "password": "bar"})
    response = await http_server_client.fetch(
        "/login", method="POST", body=data, follow_redirects=False, raise_error=False
    )
    assert response.code == 302, "redirect"
    return {"Cookie": response.headers["set-cookie"]}


@pytest.fixture
async def ws_url(http_server_port, auth_cookies):
    base_url = f"ws://localhost:{http_server_port[1]}/ws"
    auth_cookies = await auth_cookies
    request = httpclient.HTTPRequest(base_url, headers=auth_cookies)
    return request


@pytest.fixture(scope="session")
def db():
    from alembic.command import upgrade as alembic_upgrade
    from alembic.config import Config as AlembicConfig

    file = "tests/test-foo.sqlite3"
    if os.path.isfile(file):
        os.unlink(file)
    db_url = settings.get_settings("TEST_DB_URL", f"sqlite:///{file}")
    alembic_config = AlembicConfig("tests/alembic.ini")
    alembic_config.set_main_option("sqlalchemy.url", db_url)
    alembic_config.set_main_option("script_location", "tests/database/foo")
    alembic_upgrade(alembic_config, "head")
    return db_url


@pytest.fixture
def db_session(db):
    with session_scope("default", db) as session:
        yield session


@pytest.fixture
def docker_sqs(docker_services):
    docker_services.start("sqs")
    public_port = docker_services.wait_for_service("sqs", 9324)
    url = f"http://{docker_services.docker_ip}:{public_port}"
    # url = f"http://sqs:{public_port}"
    print("sqs url:", url)
    settings.SETTINGS.setdefault("endpoints", {})["sqs"] = url
    return url


@pytest.fixture
def docker_sns(docker_services):
    docker_services.start("sns")
    public_port = docker_services.wait_for_service("sns", 9911)
    url = f"http://{docker_services.docker_ip}:{public_port}"
    # url = f"http://sns:{public_port}"
    print("sns url:", url)
    settings.SETTINGS.setdefault("endpoints", {})["sns"] = url
    return url


def redis_service_checker(ip_address, port):
    # if service is ready
    r = redis.Redis(host=ip_address, port=port, db=0)
    r.set("foo", "bar")
    return True
    # otherwise return False


@pytest.fixture
def docker_redis(docker_services):
    """ run up a docker sqs """
    docker_services.start("redis")
    public_port = docker_services.wait_for_service("redis", 6379, redis_service_checker)
    uri = f"redis://{docker_services.docker_ip}:{public_port}"
    print("redis uri:", uri)
    return uri
