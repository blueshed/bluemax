from bluemax.moo import Moo
from bluemax import settings


async def test_moo():
    settings.SETTINGS.clear()
    async with Moo("tests.foo") as cow:
        result = await cow.add(2,2)
        assert result == 4