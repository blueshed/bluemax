import asyncio
from bluemax.rpc import PoolRpc, ContextRpc, context


async def test_pool():
    rpc = PoolRpc.rpc_for("tests.foo.procedures")
    result = await rpc.perform("add", 2, 2)
    assert result == 4
    result = await rpc.perform("next_second", 1)
    assert result == 2
    await rpc.shutdown()


async def test_context():
    rpc = ContextRpc.rpc_for("tests.foo.procedures")
    user = {"name": "John Doe"}

    result = await rpc.perform(user, "add", 2, 2)
    assert result == 4

    try:
        result = await rpc.perform(user, "add", 2)
    except Exception as ex:
        assert str(ex) == "add() missing 1 required positional argument: 'second'"

    result = await rpc.perform(user, "next_second", 1)
    assert result == 2

    try:
        result = await rpc.perform(user, "next_second", "a")
    except Exception as ex:
        assert str(ex) == 'can only concatenate str (not "int") to str'

    result = await rpc.perform(user, "get_sync_user")
    assert context.current_user() == None
    assert result == user

    result = await rpc.perform(user, "get_async_user")
    assert context.current_user() == None
    assert result == user

    assert context._MANAGER_ == rpc

    await rpc.shutdown()


async def test_broadcast():
    rpc = ContextRpc.rpc_for("tests.foo.procedures")
    user = {"name": "John Doe"}
    message_result = ("test", "message", None)

    result = await rpc.perform(user, "publish", "message")
    assert result == None
    assert rpc._broadcast_q_.get_nowait() == message_result

    result = await rpc.perform(user, "apublish", "message")
    assert result == None
    assert rpc._broadcast_q_.get_nowait() == message_result

    result = await rpc.perform(user, "crud", "message")
    assert result == None
    await asyncio.sleep(0.1)
    assert rpc._broadcast_q_.get_nowait() == message_result

    result = await rpc.perform(user, "acrud", "message")
    assert result == None
    await asyncio.sleep(0.1)
    assert rpc._broadcast_q_.get_nowait() == message_result

    result = await rpc.perform(user, "can_i_add", 2)
    assert result == 6

    await rpc.shutdown()
