import asyncio
import time
from concurrent.futures import ThreadPoolExecutor, ProcessPoolExecutor
from bluemax import AsyncPoolExecutor, NotRunningException


async def badwaiter(delay=1):
    print(delay)
    raise Exception("I'm bad")


async def awaiter(delay=1):
    print(delay)
    await asyncio.sleep(delay)
    return delay


async def acurry(delay=1, flavour="hot"):
    print(delay, flavour)
    await asyncio.sleep(delay)
    return delay, flavour


def waiter(delay=1):
    time.sleep(delay)
    return delay


async def test_async_work():
    pool = AsyncPoolExecutor()
    pool.start()
    work1 = pool.submit(awaiter)
    work2 = pool.submit(badwaiter)
    await pool.shutdown()
    assert work1.result() == 1
    assert str(work2.exception()) == "I'm bad"
    try:
        pool.submit(awaiter)
        assert False, "should not see this"
    except NotRunningException:
        pass
    try:
        pool.map(awaiter, [1, 1])
        assert False, "should not see this"
    except NotRunningException:
        pass


async def test_shutdown_no_wait():
    pool = AsyncPoolExecutor()
    pool.start()
    work1 = pool.submit(awaiter)
    work2 = pool.submit(badwaiter)
    await pool.shutdown(False)
    assert work1.cancelled() is True
    assert work2.cancelled() is True
    assert pool.running is False


async def test_async_pool():
    async with AsyncPoolExecutor() as pool:
        work = pool.map(awaiter, [1, 1])
    assert [1, 1] == [item.result() for item in work]


async def test_async_curry():
    async with AsyncPoolExecutor() as pool:
        work = pool.map(acurry, [1, 1], ["spicy", "mild"])
    assert [(1, "spicy"), (1, "mild")] == [item.result() for item in work]


def test_thread_pool():
    with ThreadPoolExecutor(max_workers=1) as pool:
        work = pool.submit(waiter)
    print(work.result())


def test_process_pool():
    with ProcessPoolExecutor(max_workers=1) as pool:
        work = pool.submit(waiter)
    print(work.result())
