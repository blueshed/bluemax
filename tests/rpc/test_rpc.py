from bluemax.rpc import Rpc, AsyncRpc
from tests.foo import procedures


def reverse(what: str) -> str:
    """ simple function to reverse a string """
    return what[::-1]


def test_reversed():
    """
    test that all can be called
    because not only public functions need to be available
    to call via perform.
    """
    rpc = Rpc(procedures)
    result = rpc.perform("tests.rpc.test_rpc:reverse", "olleh")
    assert result == "hello"


def test_add():
    rpc = Rpc(procedures)
    result = rpc.perform("add", 2, 2)
    assert result == 4

    assert rpc.procedures == procedures.__all__


def test_add_exception():
    rpc = Rpc(procedures)
    try:
        rpc.perform("add", 2)
        assert False, "should not get here"
    except TypeError:
        pass
    try:
        rpc.perform("add", 2, "a")
        assert False, "should not get here"
    except TypeError:
        pass


async def test_async():
    rpc = AsyncRpc.rpc_for("tests.foo.procedures")
    result = await rpc.perform("add", 2, 2)
    assert result == 4
    result = await rpc.perform("next_second", 1)
    assert result == 2
