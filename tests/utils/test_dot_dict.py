from bluemax.utils import dot_dict


def test_dot_dict():
    data = {"foo": "bar"}
    dct = dot_dict.DotDict(data)
    assert dct.foo == "bar"
