import os
from bluemax import settings
import waddle


def extend(value):
    value["test"] = "tested"


def test_extend():
    value = {}
    settings.extend("tests.utils.test_settings:extend", value)
    assert value["test"] == "tested"


def test_get_settings():
    settings.SETTINGS = {}
    settings.init_settings("tests/conf/local.yml")
    assert settings.get_settings("redis_broadcast_q") == "broadcast"
    os.environ["config"] = "tests/conf/local.yml"
    settings.SETTINGS.clear()
    assert settings.get_settings("redis_broadcast_q") == "broadcast"
    del os.environ["config"]
    settings.SETTINGS.clear()


def test_env():
    os.environ["test_env_blueq"] = "bert"
    assert settings.get_settings("test_env_blueq") == "bert"



def test_waddle():
    sample = {
        "tornado":{
            "static_dir": "foo"
        }
    }
    w_sample = waddle.ParamBunch(sample)
    tornado = w_sample.setdefault("tornado",{})
    assert isinstance(tornado, dict)
    assert tornado["static_dir"] == "foo"