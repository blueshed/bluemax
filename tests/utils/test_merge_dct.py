# pylint: disable=C0102
from bluemax.settings import merge_dict


def test_merge():
    foo = {"a": 1, "b": {"c": "d"}}
    bar = {"a": 1, "b": {"c": "e"}}

    merge_dict(foo, bar)
    assert foo["b"]["c"] == "e"


def test_create():
    foo = {"a": 1}
    bar = {"a": 1, "b": {"c": "e"}}

    merge_dict(foo, bar)
    assert foo["b"]["c"] == "e"


def test_replace():
    foo = {"a": 1, "b": 2}
    bar = {"a": 1, "b": {"c": "e"}}

    merge_dict(foo, bar)
    assert foo["b"]["c"] == "e"  # pylint: disable=E1136
