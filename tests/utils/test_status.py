import asyncio
from bluemax.utils.status import Status

STORE = []


def callback(value):
    STORE.append(value)


async def test_status(io_loop):
    status = Status(io_loop, callback)
    status["foo"] = "bar"
    await asyncio.sleep(0.11)
    assert len(STORE) == 1
    print(STORE)
    assert STORE[0]["foo"] == "bar"
