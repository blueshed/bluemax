from datetime import datetime, date
from decimal import Decimal
from bluemax.utils import json_utils


class DotDict(dict):
    """
        A dict that allows for object-like property access syntax.
    """

    def __getattr__(self, name):
        try:
            return self[name]
        except KeyError:
            raise AttributeError(name)


class Foo:
    def to_json(self):
        return {"_type_": "Foo"}


def test_datetime():
    dt = datetime(2018, 11, 11, 10, 30, 42)
    dtf = dt.isoformat().replace("T", " ")
    assert f'"{dtf}"' == json_utils.dumps(dt)


def test_date():
    dt = date(2018, 11, 11)
    dtf = dt.isoformat().replace("T", " ")
    assert f'"{dtf}"' == json_utils.dumps(dt)


def test_decimal():
    value = Decimal("42.1")
    assert f"{value}" == json_utils.dumps(value)


def test_to_json():
    data = json_utils.dumps(Foo())
    data_str = '{"_type_": "Foo"}'
    assert data_str == data


def test_doc_dict():
    data = json_utils.dumps(Foo())
    data_str = '{"_type_": "Foo"}'
    assert data_str == data
    obj = json_utils.loads(data_str, object_hook=DotDict)
    assert obj._type_ == "Foo"
    assert obj.__class__.__name__ == "DotDict"
