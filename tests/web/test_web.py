import logging
from urllib.parse import urlencode
from tornado.escape import json_decode, json_encode
from bluemax.web.server import BROADCASTER
from bluemax import context


async def test_add(http_server_client, auth_cookies, caplog):
    caplog.set_level(logging.DEBUG)

    message = {"jsonrpc": "2.0", "id": "1a", "method": "add", "params": [2, 2]}
    response = await http_server_client.fetch(
        "/rpc", method="POST", body=json_encode(message), raise_error=False
    )
    assert response.code == 403
    auth_cookies = await auth_cookies
    response = await http_server_client.fetch(
        "/rpc", method="POST", body=json_encode(message), headers=auth_cookies
    )
    assert json_decode(response.body)["result"] == 4

    await context._MANAGER_.shutdown()
    await BROADCASTER.stop()


async def test_logout(http_server_client, auth_cookies, caplog):
    caplog.set_level(logging.DEBUG)
    auth_cookies = await auth_cookies
    response = await http_server_client.fetch(
        "/logout", headers=auth_cookies, follow_redirects=False, raise_error=False
    )
    print(response)
    assert response.code == 302

    await context._MANAGER_.shutdown()
    await BROADCASTER.stop()


async def test_login_error(http_server_client, caplog):
    caplog.set_level(logging.DEBUG)
    data = urlencode({"username": "foo", "password": "bert"})
    response = await http_server_client.fetch(
        "/login", method="POST", body=data, raise_error=False
    )
    print(response)
    assert response.code == 403

    data = urlencode({"username": "foo"})
    response = await http_server_client.fetch(
        "/login", method="POST", body=data, raise_error=False
    )
    print(response)
    assert response.code == 403

    await context._MANAGER_.shutdown()
    await BROADCASTER.stop()
