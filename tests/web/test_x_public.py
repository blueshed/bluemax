import pytest
from bluemax import context
from bluemax import settings
from bluemax.web.server import make_app, BROADCASTER


@pytest.fixture
def app():
    settings.SETTINGS.clear()
    settings.SETTINGS.update(
        {
            "procedures": "tests.foo.procedures",
            "tornado": {
                "auth_handler": None,
                "manager": "bluemax:ContextRpc",
                "static_dir": "tests/static",
            },
            "redis_url": None,
        }
    )
    result = make_app()
    print("running public app")
    return result


async def test_public(http_server_client):
    response = await http_server_client.fetch(
        "/", method="GET", follow_redirects=False, raise_error=False
    )
    assert response.code == 200
    await context._MANAGER_.shutdown()
    await BROADCASTER.stop()
