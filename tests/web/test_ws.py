import logging
import tornado.web
from tornado.escape import json_decode, json_encode
from bluemax.web.server import BROADCASTER
from bluemax import context


async def test_add(http_server_client, ws_url, caplog):
    caplog.set_level(logging.DEBUG)
    ws_url = await ws_url
    print(repr(ws_url))
    ws_client = await tornado.websocket.websocket_connect(ws_url)
    user = await ws_client.read_message()
    print(user)
    message = {"jsonrpc": "2.0", "id": "1a", "method": "add", "params": [2, 2]}
    await ws_client.write_message(json_encode(message))
    response = await ws_client.read_message()
    print(response)
    assert json_decode(response)["result"] == 4

    await context._MANAGER_.shutdown()
    ws_client.close()
    await BROADCASTER.stop()
