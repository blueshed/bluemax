import asyncio
import logging
import pytest
import tornado.websocket
import bluemax
from bluemax import context
from bluemax import settings
from bluemax.web.server import make_app, BROADCASTER
from tests.foo.services import clock


@pytest.fixture
def app(io_loop):
    settings.SETTINGS.clear()
    settings.SETTINGS.update(
        {
            "procedures": "tests.foo.procedures",
            "services": "tests.foo.services",
            "tornado": {
                "auth_handler": "tests.foo.urls:SampleAuth",
                "manager": "bluemax.services:ServicesRpc",
                "static_dir": None,
            },
            "redis_url": None,
        }
    )
    result = make_app()
    return result


async def test_clock():
    broadcasted = []

    def broadcast(signal, message):
        broadcasted.append((signal, message))

    b_cache = bluemax.context.broadcast
    bluemax.context.broadcast = broadcast
    asyncio.ensure_future(clock())
    await asyncio.sleep(3)
    assert len(broadcasted) == 1
    bluemax.context.broadcast = b_cache


async def test_local_clock(http_server_client, ws_url, caplog):
    caplog.set_level(logging.DEBUG)
    ws_url = await ws_url
    ws_client = await tornado.websocket.websocket_connect(ws_url)
    await ws_client.read_message()  # user
    response = await ws_client.read_message()
    print(response)

    await context._MANAGER_.shutdown()
    ws_client.close()
    await BROADCASTER.stop()
