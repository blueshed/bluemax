import logging
import pytest
from bluemax import context
from bluemax import settings
from bluemax.web.server import make_app, BROADCASTER


@pytest.fixture
def app(io_loop):
    settings.SETTINGS.clear()
    settings.SETTINGS.update(
        {
            "procedures": "tests.foo.procedures",
            "tornado": {
                "auth_handler": "tests.foo.urls:SampleAuth",
                "manager": "bluemax:ContextRpc",
                "static_dir": "tests/static",
            },
            "redis_url": None,
        }
    )
    return make_app()


async def test_static(http_server_client, auth_cookies, caplog):
    caplog.set_level(logging.DEBUG)
    auth_cookies = await auth_cookies
    response = await http_server_client.fetch(
        "/", method="GET", raise_error=False, headers=auth_cookies
    )
    assert response.code == 200
    await context._MANAGER_.shutdown()
    await BROADCASTER.stop()
