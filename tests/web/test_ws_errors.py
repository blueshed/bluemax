import logging
import tornado.web
from tornado.escape import json_decode, json_encode
from bluemax.web.server import BROADCASTER
from bluemax import context


async def test_add(http_server_client, ws_url, caplog):
    caplog.set_level(logging.DEBUG)
    ws_url = await ws_url
    ws_client = await tornado.websocket.websocket_connect(ws_url)
    user = await ws_client.read_message()
    print(user)

    message = {
        "jsonrpc": "2.0",
        "id": "1a",
        "method": "add",
        "params": {"first": 4, "second": 6},
    }
    await ws_client.write_message(json_encode(message))
    response = await ws_client.read_message()
    assert json_decode(response)["result"] == 10

    message = {"jsonrpc": "2.0", "method": "add", "params": {"first": 4, "second": 6}}

    await ws_client.write_message(json_encode(message))
    message = {"jsonrpc": "2.0", "id": "2a", "method": "add", "params": "4,6"}
    await ws_client.write_message(json_encode(message))
    response = await ws_client.read_message()
    assert json_decode(response)["error"]["code"] == -32602

    await context._MANAGER_.shutdown()
    ws_client.close()
    await BROADCASTER.stop()


async def test_error(http_server_client, ws_url, caplog):
    caplog.set_level(logging.DEBUG)
    ws_url = await ws_url
    ws_client = await tornado.websocket.websocket_connect(ws_url)
    user = await ws_client.read_message()
    message = {"jsonrpc": "2.0", "id": "1a", "method": "add", "params": [2, "a"]}
    await ws_client.write_message(json_encode(message))
    response = await ws_client.read_message()
    assert json_decode(response)["error"]["code"] == -32000

    message = {"jsonrpc": "2", "id": "1a", "method": "add", "params": [2, 2]}
    await ws_client.write_message(json_encode(message))
    response = await ws_client.read_message()
    assert json_decode(response)["error"]["code"] == -32600

    await context._MANAGER_.shutdown()
    ws_client.close()
    await BROADCASTER.stop()


async def test_missing_method(http_server_client, ws_url, caplog):
    caplog.set_level(logging.DEBUG)
    ws_url = await ws_url
    ws_client = await tornado.websocket.websocket_connect(ws_url)
    user = await ws_client.read_message()
    message = {"jsonrpc": "2.0", "id": "1a", "params": [2, "a"]}
    await ws_client.write_message(json_encode(message))
    response = await ws_client.read_message()
    assert json_decode(response)["error"]["code"] == -32600

    message = {"jsonrpc": "2.0", "id": "1a", "method": "adder", "params": [2, 2]}
    await ws_client.write_message(json_encode(message))
    response = await ws_client.read_message()
    assert json_decode(response)["error"]["code"] == -32600

    message = {"jsonrpc": "2.0", "id": "1a", "method": "_add", "params": [2, 2]}
    await ws_client.write_message(json_encode(message))
    response = await ws_client.read_message()
    assert json_decode(response)["error"]["code"] == -32600

    await context._MANAGER_.shutdown()
    ws_client.close()
    await BROADCASTER.stop()
