import logging
import tornado.web
from tornado.escape import json_decode, json_encode
from bluemax.web.server import BROADCASTER
from bluemax import context


async def test_broadcast(http_server_client, ws_url):
    ws_url = await ws_url
    print(repr(ws_url))
    ws_client = await tornado.websocket.websocket_connect(ws_url)
    await ws_client.read_message()
    message = {
        "jsonrpc": "2.0",
        "id": "1a",
        "method": "can_i_broadcast",
        "params": ["hi there!"],
    }
    await ws_client.write_message(json_encode(message))
    response = await ws_client.read_message()
    print(response)
    assert json_decode(response)["result"] == "ok"
    response = await ws_client.read_message()
    assert json_decode(response)["signal"] == "salutation"
    assert json_decode(response)["message"] == "hi there!"

    await context._MANAGER_.shutdown()
    ws_client.close()
    await BROADCASTER.stop()
