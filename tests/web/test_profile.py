from bluemax.utils import json_utils


async def test_profile_handler(http_server_client, auth_cookies):
    auth_cookies = await auth_cookies
    response = await http_server_client.fetch(
        "/profile", headers=auth_cookies, follow_redirects=False, raise_error=False
    )
    print(response)
    assert response.code == 200
    user = json_utils.loads(response.body)
    assert user == {"user": "foo"}
