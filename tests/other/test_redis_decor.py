import asyncio
import json
import logging
from bluemax.work import rd_session
from bluemax.work.redis_pubsub import redis_subscribe, redis_publish, redis_cmd
from bluemax import settings

results = []
KEY = "workers_queue"


async def test_handle_work(docker_redis, caplog):
    caplog.set_level(logging.DEBUG)
    await rd_session.shutdown()
    settings.SETTINGS.clear()
    settings.SETTINGS["redis_url"] = docker_redis

    @redis_subscribe(key=KEY)
    def handle_work(message=None):
        document = json.loads(message)
        results.append(document)

    @redis_subscribe()
    def workers_queue(message=None):
        document = json.loads(message)
        results.append(document)

    @redis_publish(key=KEY)
    def publish_work(document):
        return json.dumps(document)

    @redis_publish(key=KEY)
    async def async_publish_work(document):
        return json.dumps(document)

    mine = []

    def another_way(message):
        document = json.loads(message)
        mine.append(document)

    """ not so pretty but don't forget to call it! """
    redis_subscribe(another_way, key=KEY)()

    # start our subscriptions
    handle_work()
    workers_queue()
    await asyncio.sleep(0.1)

    document = {"message": "hi there"}
    publish_work(document)
    await asyncio.sleep(0.1)
    assert results[-1] == document
    assert len(results) == 2

    await async_publish_work(document)
    await asyncio.sleep(0.1)
    assert results[-1] == document
    assert len(results) == 4

    assert len(mine) == 2

    await rd_session.shutdown()


async def test_rpush_blpop(docker_redis, caplog):
    caplog.set_level(logging.DEBUG)
    await rd_session.shutdown()
    settings.SETTINGS.clear()
    settings.SETTINGS["redis_url"] = docker_redis

    @redis_cmd(key="do_work", cmd="blpop", cmd_args=[0])
    def done_work(message=None):
        document = json.loads(message[1].decode("utf-8"))
        results.append(document)

    @redis_publish(cmd="rpush")
    def do_work(message):
        return json.dumps(message)

    # start our subscriptions
    done_work()
    await asyncio.sleep(0.1)

    document = {"message": "hi there 2"}
    do_work(document)
    await asyncio.sleep(0.1)
    assert results[-1] == document

    await rd_session.shutdown()


async def test_classes(docker_redis, caplog):
    caplog.set_level(logging.DEBUG)
    await rd_session.shutdown()
    settings.SETTINGS.clear()
    settings.SETTINGS["redis_url"] = docker_redis
    logging.info("settings: %s", settings.SETTINGS)

    class Foo:
        results = []

        def __init__(self):
            self.done_work()  # subscribe

        @redis_cmd(key="do_class_work", cmd="blpop", cmd_args=[0])
        def done_work(self, message=None):
            document = json.loads(message[1].decode("utf-8"))
            self.results.append(document)

        @redis_publish(cmd="rpush")
        def do_class_work(self, message):
            return json.dumps(message)

    foo = Foo()
    await asyncio.sleep(0.1)

    document = {"message": "hi there 2"}
    foo.do_class_work(document)
    await asyncio.sleep(0.1)
    assert foo.results[-1] == document

    await rd_session.shutdown()
