import sqlalchemy as sa
from bluemax.sa import serialize, db_session
from tests.foo.model import *


def test_model(db_session):
    person = Person(name="John Doe", password="felix")
    db_session.add(person)
    db_session.commit()
    assert person.id == 1

    role = Role(name="Worker")
    db_session.add(role)
    role.activites.append(Activity(name="Type"))
    role.activites.append(Activity(name="Tidy"))
    role.activites.append(Activity(name="Tutor"))
    person.role = role
    db_session.commit()

    assert ["Type", "Tidy", "Tutor"] == [act.name for act in person.role.activites]
    assert ["Type", "Tidy", "Tutor"] == [
        row.name
        for row in db_session.query(Activity.name)
        .join(Role.activites)
        .join(Person)
        .filter(Person.id == 1)
        .order_by(Activity.id)
    ]

    assert db_session.query(Activity).get(3).name == "Tutor"


def test_more_model(db_session):
    tutor = db_session.query(Activity).get(3)
    assert tutor.name == "Tutor"
    assert serialize.dumps(tutor) == {"_type_": "Activity", "id": 3, "name": "Tutor"}

    person2 = Person()
    db_session.add(person2)
    serialize.loads({"name": "Jane Doe", "password": "olive"}, person2)
    db_session.commit()
    assert person2.id == 2
    assert person2.name == "Jane Doe"
    assert person2.password == "olive"


def test_private_properties(db_session):
    person = Person()
    db_session.add(person)
    serialize.loads(
        {"name": "Di Doe", "password": "gwenna", "_secret": "plastics"}, person
    )
    db_session.commit()
    assert person._secret is None
    person._secrets = "carbon fiber"
    db_session.commit()
    assert serialize.dumps(person) == {
        "_type_": "Person",
        "id": 3,
        "name": "Di Doe",
        "password": "gwenna",
        "role_id": None,
    }


def test_error(db):
    try:
        with db_session.session_scope("default") as session:
            person = Person(id=2)
            session.add(person)
            session.commit()
            assert False, "should throw IntegrityError"
    except sa.exc.IntegrityError:
        pass
