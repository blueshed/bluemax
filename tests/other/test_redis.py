import asyncio
import logging
import pytest
import tornado.web
from bluemax import settings
from bluemax.web.server import make_app, BROADCASTER
from bluemax.utils import json_utils
from bluemax.work.worker import Worker
from bluemax.work import rd_session

MANAGER = None


@pytest.fixture
def app(docker_redis):
    global MANAGER  # pylint: disable=W0603
    settings.SETTINGS.clear()
    settings.SETTINGS.update(
        {
            "procedures": "tests.foo.procedures",
            "services": "tests.foo.services",
            "tornado": {
                "auth_handler": None,
                "manager": "bluemax.work:RedisManager",
                "static_dir": "tests/static",
            },
            "redis_url": docker_redis,
            "max_workers": 2,
        }
    )
    result = make_app()
    print("running public app")
    MANAGER = result.settings["manager"]
    return result


async def test_worker(docker_redis, http_server_port, http_server_client, caplog):
    caplog.set_level(logging.DEBUG)

    max_workers = settings.get_settings("max_workers")
    worker = Worker.rpc_for("tests.foo.procedures", max_workers=max_workers)
    print(worker)
    await asyncio.sleep(1)

    ws_url = f"ws://localhost:{http_server_port[1]}/ws"
    print(repr(ws_url))
    ws_client = await tornado.websocket.websocket_connect(ws_url)

    message = {"jsonrpc": "2.0", "id": "1a", "method": "add", "params": [2, 2]}
    await ws_client.write_message(json_utils.dumps(message))
    response = await ws_client.read_message()
    print(response)
    assert json_utils.loads(response)["result"] == 4

    message = {
        "jsonrpc": "2.0",
        "id": "1a",
        "method": "can_i_broadcast",
        "params": ["hi there!"],
    }
    await ws_client.write_message(json_utils.dumps(message))
    response = await ws_client.read_message()
    print(response)
    assert json_utils.loads(response)["result"] == "ok"
    response = await ws_client.read_message()
    assert json_utils.loads(response)["signal"] == "salutation"
    assert json_utils.loads(response)["message"] == "hi there!"

    ws_client.close()
    await MANAGER.shutdown()
    await worker.shutdown()
    await BROADCASTER.stop()
    await rd_session.shutdown()
