from sqlalchemy import Column, String, Integer, ForeignKey
from sqlalchemy.orm import relationship
from .base import Base

class Person(Base):
    
    id = Column(Integer, primary_key=True)
    name = Column(String(255))
    password = Column(String(255))
    role_id = Column(Integer, ForeignKey('role.id'))
    role = relationship('Role', uselist=False,
        primaryjoin='Person.role_id==Role.id', 
        remote_side='Role.id',
        back_populates='people')
    _secret = Column(String(12), unique=True)
