from sqlalchemy import String, Integer, Table, Column, ForeignKey
from sqlalchemy.orm import relationship
from .base import Base

role_activites_activity = Table('role_activites_activity', Base.metadata,
    Column('activites_id', Integer, ForeignKey('role.id')),
    Column('activity_id', Integer, ForeignKey('activity.id')),
    mysql_comment='{\"back_populates\":\"Role.activites\"}',
    mysql_engine='InnoDB')


class Role(Base):
    
    id = Column(Integer, primary_key=True)
    name = Column(String(255))
    activites = relationship('Activity',
        primaryjoin='Role.id==role_activites_activity.c.activites_id',
        secondaryjoin='Activity.id==role_activites_activity.c.activity_id',
        secondary='role_activites_activity',
        lazy='joined')
    people = relationship('Person', uselist=True, 
        primaryjoin='Person.role_id==Role.id',
        remote_side='Person.role_id',
        back_populates='role')

