from .base import Base
from sqlalchemy import Column, String, Integer


class Activity(Base):
    
    id = Column(Integer, primary_key=True)
    name = Column(String(255))

