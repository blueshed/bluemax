""" Used by alembic """
from sqlalchemy.ext.declarative import declarative_base, declared_attr


class Base:
    """ Base for orm model classes """

    @declared_attr
    def __tablename__(cls):
        """ with this tablename is class name lower"""
        return cls.__name__.lower()


Base = declarative_base(cls=Base)
