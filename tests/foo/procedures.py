"""
    Simple functions
"""
import asyncio
from bluemax import context

__all__ = ["add", "can_i_broadcast"]


def add(first: int, second: int) -> int:
    """ the sum of two integers """
    return first + second


async def next_second(time):
    """ takes a second to add one to time parameter """
    await asyncio.sleep(1)
    return time + 1


def get_sync_user():
    """ return the current user """
    return context.current_user()


async def get_async_user():
    """ return the current user """
    return context.current_user()


def publish(message):
    """ simple broadcaster """
    context.broadcast("test", message)


async def apublish(message):
    """ simple async broadcaster """
    context.broadcast("test", message)


def crud(message):
    """ simple broadcaster on success """
    context.broadcast_on_success("test", message)


async def acrud(message):
    """ simple async broadcaster on success """
    context.broadcast_on_success("test", message)


async def can_i_add(amount):
    result = await context.perform("add", 2, 2)
    assert result == 4, "it used to!"
    return result + amount


async def can_i_broadcast(message):
    context.broadcast("salutation", message)
    return "ok"
