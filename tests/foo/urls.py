""" add your tornado routes here """
import logging
from bluemax.web.authentication import LoginHandler


class SampleAuth(LoginHandler):
    async def login(self, username, password):
        if username == "foo" and password == "bar":
            return {"user": username}


def extend(urls):
    logging.info("extending urls")
    return urls
