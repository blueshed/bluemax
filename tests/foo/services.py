""" an example service """
import asyncio
import time
import logging
from bluemax import context


async def clock():
    """ Will broadcast every 5 seconds """
    context.broadcast("clock", {"message": "started"})
    total = 0
    try:
        while True:
            now = time.time()
            logging.debug("calling add")
            total = await context.perform("add", total, now)
            context.broadcast("time", {"now": now, "total": total})
            await asyncio.sleep(5)
    except asyncio.CancelledError:
        context.broadcast("clock", {"message": "stopped"})
