""" an example log setup """
import os
import yaml


def extend(config):
    if os.path.isfile("logging.yml"):
        print("loading logging.yml")
        with open("logging.yml", "r") as file:
            return yaml.safe_load(file)
