import logging
import waddle
from bluemax import settings

def test_settings(caplog):
    caplog.set_level(logging.DEBUG)
    settings.SETTINGS = waddle.ParamBunch(values={
        "foo": "bar",
        "bert": {
            "john": "paul",
            "ringo": "george"
        }
    })
    assert settings.SETTINGS.bert.john == "paul"
    assert settings.SETTINGS.get("bert").get("john") == "paul"
    assert settings.get_settings("bert.john") == "paul"
    settings.SETTINGS = {}