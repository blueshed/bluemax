import asyncio
import logging
from bluemax import settings
from bluemax.settings import SETTINGS
from bluemax.utils import json_utils
from bluemax.aws.aws_pubsub import AwsCache, aws_subscribe

LOGGER = logging.getLogger(__name__)


def allow_sns_to_write_to_sqs(topic_arn, queue_arn):
    return f'''{{
  "Version": "2012-10-17",
  "Statement": [
    {{
      "Sid": "topic-queue-link-policy",
      "Effect": "Allow",
      "Principal": {{"AWS" : "*"}},
      "Action": "SQS:SendMessage",
      "Resource": "{queue_arn}",
      "Condition": {{
        "ArnEquals": {{
          "aws:SourceArn": "{topic_arn}"
        }}
      }}
    }}
  ]
}}'''


async def test_topic(docker_sns, docker_sqs, caplog):
    caplog.set_level(logging.INFO)
    AwsCache.clients.clear()
    SETTINGS.setdefault("endpoints",{})["sns"] = docker_sns
    SETTINGS.setdefault("endpoints",{})["sqs"] = docker_sqs
    stage = settings.get_settings('stage')
    sns = AwsCache.client('sns')
    response = await sns.create_topic(Name=f't-handle-{stage}')
    LOGGER.debug(response)
    topic_arn = response.get('TopicArn')

    sqs = AwsCache.client('sqs')
    response = await sqs.create_queue(QueueName=f'q-handle-{stage}')
    LOGGER.info(response)
    queue_url = response.get('QueueUrl')

    response = await sqs.get_queue_attributes(
        QueueUrl=queue_url, AttributeNames=['QueueArn']
    )
    LOGGER.info(response)
    queue_arn = response['Attributes']['QueueArn']
    policy_json = allow_sns_to_write_to_sqs(topic_arn, queue_arn)
    response = await sqs.set_queue_attributes(
        QueueUrl=queue_url, Attributes={'Policy': policy_json}
    )
    LOGGER.info(response)

    queue_arn = f'aws-sqs://q-handle-{stage}?amazonSQSEndpoint=http://sqs:9324&accessKey=&secretKey='
    LOGGER.info(queue_arn)
    response = await sns.subscribe(
        TopicArn=topic_arn,
        Protocol='sqs',
        Endpoint=queue_arn,
        Attributes={'RawMessageDelivery': 'true'},
    )
    LOGGER.info(response)
    subscription_arn = response['SubscriptionArn']
    done = []

    @aws_subscribe(queue_name='q-handle')
    async def handle(message=None):
        LOGGER.info('got topic message: %s', message)
        message = json_utils.loads(message)
        message = message["Message"]
        done.append(message)

    handle()
    await asyncio.sleep(1)
    response = await AwsCache.apublish('t-handle', message='foo')
    LOGGER.debug(response)
    await asyncio.sleep(2)
    try:
        assert len(done) == 1
        assert done[0] == 'foo'
    finally:
        response = await sns.unsubscribe(SubscriptionArn=subscription_arn)
        LOGGER.debug(response)
        response = await sns.delete_topic(TopicArn=topic_arn)
        LOGGER.debug(response)
        response = await sqs.delete_queue(QueueUrl=queue_url)
        LOGGER.debug(response)
