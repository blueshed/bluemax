import asyncio
import logging
from bluemax import settings
from bluemax.settings import SETTINGS
from bluemax.aws.aws_pubsub import AwsCache, aws_subscribe

LOGGER = logging.getLogger(__name__)

async def test_queue(docker_sqs, caplog):
    caplog.set_level(logging.INFO)
    AwsCache.clients.clear()
    SETTINGS.setdefault("endpoints",{})["sqs"] = docker_sqs
    stage = settings.get_settings("stage")
    sqs = AwsCache.client("sqs")
    response = await sqs.create_queue(QueueName=f"handle-{stage}")
    queue_url = response.get("QueueUrl")
    assert queue_url
    done = []

    @aws_subscribe
    async def handle(message=None):
        LOGGER.info("got queue message: %s", message)
        done.append(message)

    handle()
    await AwsCache.apost("handle", message="foo")
    await asyncio.sleep(2)
    try:
        assert done[0] == "foo"
    finally:
        response = await sqs.delete_queue(QueueUrl=queue_url)
        print(response)
