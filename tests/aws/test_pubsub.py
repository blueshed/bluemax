import sys
from bluemax.aws.aws_pubsub import aws_subscribe, aws_publish
from bluemax.utils.describe import describe

__all__ = ["test_queue", "test_topic"]

@aws_subscribe
def test_queue(message=None):
    """ listens to sample queue """
    pass

@aws_publish(topic_name="foo")
def test_topic(message=None):
    """ publishes to sample topic """
    return message


def test_describe():
    result = describe(sys.modules[__name__])
    print(result)
    values = list(result.values())
    print(values)
    assert values[0].name == "test_queue"
    assert values[1].name == "test_topic"