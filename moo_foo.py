import asyncio
import logging
from bluemax.moo import Moo


async def main():
    async with Moo("foo") as cow:
        result = await cow.add(2, 2)
        assert result == 4
        logging.info("yes!")


if __name__ == "__main__":
    asyncio.run(main())
