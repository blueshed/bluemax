
SHELL := /bin/bash

lint:
	source venv/bin/activate && \
		invoke lint

test:
	source venv/bin/activate && \
		invoke test

check: lint test

build:
	source venv/bin/activate && \
		python setup.py sdist

full: check build

setup:
	if which python3.7 && [ ! -d venv ] ; then python3.7 -m venv venv ; fi
	source venv/bin/activate \
	  && python -m pip install -q -U pip \
	  && pip install -e .[dev]
