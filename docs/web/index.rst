Web
===


.. automodule:: bluemax.web


.. automodule:: bluemax.web.server
    :members:


.. automodule:: bluemax.web.rpc_websocket
    :members:


.. automodule:: bluemax.web.broadcaster
    :members:


.. autoclass:: bluemax.web.auth_static_file_handler.AuthStaticFileHandler
    :members:
