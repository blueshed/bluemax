Web Authentication
==================


.. automodule:: bluemax.web.authentication


.. autoclass:: bluemax.web.authentication.cognito_auth.CognitoHandler
    :members:


.. autoclass:: bluemax.web.authentication.ldap_auth.LDAP3LoginHandler
    :members:


.. autoclass:: bluemax.web.authentication.ms_auth.MSLoginHandler
    :members:


.. autoclass:: bluemax.web.authentication.login_handler.LoginHandler
    :members:


.. autoclass:: bluemax.web.authentication.logout_handler.LogoutHandler
    :members:


.. autoclass:: bluemax.web.authentication.user_mixin.UserMixin
    :members:


.. autoclass:: bluemax.web.authentication.profile_handler.ProfileHandler
    :members:
