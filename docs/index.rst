.. bluemax documentation master file, created by
   sphinx-quickstart on Mon Aug 12 18:42:32 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to bluemax's documentation!
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   overview
   tutorial
   tasks
   rpc
   web/index
   web/authentication
   work
   services
   utils
   sa
   aws
   moo



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
