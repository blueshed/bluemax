Tutorial
========


Installing
----------

To create a bluemax project, first create a virtual envirionment in
python3.7 or greater:

.. code-block::

    python3.7 -m venv venv

then activate it with:

.. code-block::

    source venv/bin/activate

and install bluemax with pip:

.. code-block::

    pip install bluemax[dev]


First step
----------

Now you can create a new project with:

.. code-block::

    bluemax create.project foo

This will have created a new python package called `foo` beside venv.
Of course, you can name your package any valid python module name.
Inside are examples of procedures and services.

A *procedure* is a public function made available by the rpc.
It can be either sync or async as it will either be performed by a
Threadpool or an AsyncPool. The function is considered public if
it resides in the `__all__` property of the foo.procedures module.
A default `add` procedure is provided.

*Services* are background async tasks that can be controlled by
the ServiceManager; typically they'd listen for events on queues.
The service must be in the `__all__` property of the foo.services module.
A default `clock` service is provided.


To use your new functionality run ipython and type:

.. code-block::

    $ ipython
    In [1]: from bluemax.moo import Moo
    In [2]: async with Moo("foo") as cow:
       ...:     result = await cow.add(2, 2)
    In [3]: result
    Out[3]: 4


We use ipython because the regular python repl does not support async
functions in such a straight forward way.

Rpc Server
----------

To run your project as an rpc server:

.. code-block::

    bluemax run -m foo

Now you can use another terminal to curl to bluemax:

.. code-block::

    curl -d '{"jsonrpc": "2.0", "id": "1", "method": "add", "params": [2, 2]}' -X POST http://localhost:8080/rpc

and the response:

.. code-block::

    {"jsonrpc": "2.0", "id": "1", "result": 4}

NB. if you fail to provide an id for the request it will be considered a
one way request and no reply will be provided, as per the jsonrpc spec.

Websocket
---------

You can also access your functions through the websocket interface:

.. code-block::

    ws://localhost:8080/ws

To demonstrate this we can add a static_dir to your foo.settings and
add an index.html. First create a static folder and then add to foo.settings:

.. code-block::

    settings.setdefault("tornado",{})["static_dir"] = "foo/static"

The Tornado Application is initialized with routes and settings. You
can customize the routes by extending them in foo.urls.py and similarly
you can customize the settings in foo.settings.py You can overide the
logging in foo.log.py

Then the index.html:

.. code-block:: html

    <script>
        var ws = new WebSocket("ws://localhost:8080/ws")
        ws.onmessage = function(evt){
            var elem = document.getElementById("result");
            elem.prepend(document.createElement("br"));
            elem.prepend(document.createTextNode(evt.data));
        }
        ws.onopen = function(){
            ws.send('{"jsonrpc": "2.0", "id": "1", "method": "add", "params": [2, 2]}')
        }
    </script>
    <div id="result"></div>


Then run you server again, but with services:

.. code-block::

    bluemax run -m foo -s foo.services

and go to http://localhost:8080 in your browser. You should see:

.. code-block::

    {"signal": "time", "message": {"now": 1565703251.6646788}}
    {"jsonrpc": "2.0", "id": "1", "result": 4}

The rpc call came back first and then a signal from the clock
service.

Redis
-----

You can run multiple workers by using redis. Bring up a redis Server
and add it's address to foo.settings.py:

.. code-block::

    settings["redis_url"] = "redis://localhost:6379/0"
    settings.setdefault("tornado", {})["manager"] = "bluemax.work:RedisManager"

Then in one terminal run:

.. code-block::

    bluemax run.server -m foo

in another run:

.. code-block::

    bluemax run.worker -m foo

in yet another run:

.. code-block::

    bluemax run.services -m foo -s foo.services

Reloading you web page should result in the same page... but these
you can scale independently.
