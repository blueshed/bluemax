Services
========

This package contains the classes used to provide services, background
async tasks. You'd access them through:

.. code-block::

    bluemax run -m foo -s foo.services

or independently:

    bluemax run.services -m foo -s foo.services



.. automodule:: bluemax.services
   :members:


.. autoclass:: bluemax.services.ServicesManager
    :members:


.. autoclass:: bluemax.services.ServicesRpc
    :members:


.. autoclass:: bluemax.services.server.Services
    :members:
