Work
====

This package contains the classes that run remote redis workers.
You'd access them via:

.. code-block::

    bluemax run.worker -m foo


.. autoclass:: bluemax.work.RedisManager
    :members:


.. autoclass:: bluemax.work.worker.Worker
    :members:


.. autoclass:: bluemax.work.redis_broadcaster.RedisBroadcaster
    :members:


.. automodule:: bluemax.work.redis_pubsub
    :members:


.. automodule:: bluemax.work.rd_session
    :members:
