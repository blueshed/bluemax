SqlAlchemy
==========

bluemax provides some helper tasks for working with Alembic and SqlAlchemy.
First to create a model for SqlAlchemy you can use the bluemax task:

.. code-block::

    bluemax db.create foo

This will create a model module inside the foo project and in
an database folder create an alemnic envirionment.

To add revisions:

.. code-block::

    bluemax db.revise foo -m "first pass"

To update:

.. code-block::

    bluemax db.upgrade foo

To downgrade:

.. code-block::

    bluemax db.downgrade foo base


To access your database in code, using a synchronous function,
you'll first want put the `db_url` setting in foo.settings.py
with you sqlalchemy connection string and then:

.. code-block::

    import bluemax.sa
    from bluemax import context

    def get_person(key:int) -> dict:
        with bluemax.sa.session_scope() as session:
            person = session.query(model.Person).get(key)
            return bluemax.sa.serialize.dumps(person)

    def set_person(key: int, data: dict) -> dict:
        with bluemax.sa.session_scope() as session:
            if key:
                person = session.query(model.Person).get(key)
            else:
                person = model.Person()
                session.add(person)
            bluemax.sa.serialize.loads(person, data)
            session.commit()
            result = bluemax.sa.serialize.dumps(person)
            context.broadcast_on_success("set person", result)
            return result["id"]

    def del_person(key:int) -> dict:
        with bluemax.sa.session_scope() as session:
            session.query(model.Person).filter_by(id=key).delete()
            session.commit()
            context.broadcast_on_success("del person", key)


.. automodule:: bluemax.sa
   :members:


.. automodule:: bluemax.sa.db_session
   :members:


.. automodule:: bluemax.sa.serialize
   :members:
