Overview
========

Overview of the bluemax Project.

Fun with tornado and async.


.. autofunction:: bluemax.bring_up


This is why we made bluemax. The ability to treat
async functions like those in a Thread or Process Pool
executor.


.. automodule:: bluemax.AsyncPoolExecutor
    :: members


references:

* `tornado <http://www.tornadoweb.org/>`_
* `sqlalchemy <http://www.sqlalchemy.org/>`_
